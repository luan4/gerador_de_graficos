iperf 3.0.11
Linux josue-X23 4.10.0-28-generic #32~16.04.2-Ubuntu SMP Thu Jul 20 10:19:48 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
Time: Sat, 15 Dec 2018 23:03:10 GMT
Connecting to host 192.168.43.208, port 5201
Reverse mode, remote host 192.168.43.208 is sending
      Cookie: josue-X23.1544914990.112328.1089d2e0
[  4] local 192.168.43.14 port 48110 connected to 192.168.43.208 port 5201
Starting Test: protocol: UDP, 1 streams, 8192 byte blocks, omitting 0 seconds, 10 second test
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-1.00   sec  1.20 MBytes  10.1 Mbits/sec  2.471 ms  0/154 (0%)  
[  4]   1.00-2.00   sec  1.19 MBytes  9.96 Mbits/sec  1.844 ms  0/152 (0%)  
[  4]   2.00-3.00   sec  1.20 MBytes  10.0 Mbits/sec  3.079 ms  0/153 (0%)  
[  4]   3.00-4.00   sec  1.19 MBytes  9.96 Mbits/sec  3.223 ms  0/152 (0%)  
[  4]   4.00-5.00   sec  1.20 MBytes  10.0 Mbits/sec  2.911 ms  0/153 (0%)  
[  4]   5.00-6.00   sec  1.19 MBytes  9.96 Mbits/sec  3.082 ms  0/152 (0%)  
[  4]   6.00-7.00   sec  1.20 MBytes  10.0 Mbits/sec  2.801 ms  0/153 (0%)  
[  4]   7.00-8.00   sec  1.20 MBytes  10.0 Mbits/sec  2.717 ms  0/153 (0%)  
[  4]   8.00-9.00   sec  1.19 MBytes  9.96 Mbits/sec  2.427 ms  0/152 (0%)  
[  4]   9.00-10.00  sec  1.20 MBytes  10.0 Mbits/sec  2.710 ms  0/153 (0%)  
- - - - - - - - - - - - - - - - - - - - - - - - -
Test Complete. Summary Results:
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  11.9 MBytes  10.0 Mbits/sec  2.710 ms  0/1527 (0%)  
[  4] Sent 1527 datagrams
CPU Utilization: local/receiver 1.4% (0.2%u/1.2%s), remote/sender 2.4% (0.3%u/2.1%s)

iperf Done.
