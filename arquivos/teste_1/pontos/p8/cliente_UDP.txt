iperf 3.0.11
Linux josue-X23 4.10.0-28-generic #32~16.04.2-Ubuntu SMP Thu Jul 20 10:19:48 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
Time: Sat, 15 Dec 2018 23:06:31 GMT
Connecting to host 192.168.43.208, port 5201
Reverse mode, remote host 192.168.43.208 is sending
      Cookie: josue-X23.1544915191.959315.4c7b000e
[  4] local 192.168.43.14 port 51382 connected to 192.168.43.208 port 5201
Starting Test: protocol: UDP, 1 streams, 8192 byte blocks, omitting 0 seconds, 10 second test
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-1.00   sec  1.19 MBytes  9.96 Mbits/sec  2.751 ms  2/154 (1.3%)  
[  4]   1.00-2.00   sec  1.13 MBytes  9.50 Mbits/sec  5.390 ms  0/145 (0%)  
[  4]   2.00-3.00   sec  1.25 MBytes  10.5 Mbits/sec  2.677 ms  0/160 (0%)  
[  4]   3.00-4.00   sec  1.20 MBytes  10.0 Mbits/sec  2.181 ms  0/153 (0%)  
[  4]   4.00-5.00   sec  1.18 MBytes  9.90 Mbits/sec  2.894 ms  1/152 (0.66%)  
[  4]   5.00-6.00   sec  1.17 MBytes  9.83 Mbits/sec  3.391 ms  0/150 (0%)  
[  4]   6.00-7.00   sec  1.21 MBytes  10.2 Mbits/sec  3.141 ms  0/155 (0%)  
[  4]   7.00-8.00   sec  1.20 MBytes  10.0 Mbits/sec  2.811 ms  0/153 (0%)  
[  4]   8.00-9.00   sec  1.07 MBytes  8.98 Mbits/sec  1.434 ms  0/137 (0%)  
[  4]   9.00-10.00  sec  1.20 MBytes  10.0 Mbits/sec  4.735 ms  0/153 (0%)  
- - - - - - - - - - - - - - - - - - - - - - - - -
Test Complete. Summary Results:
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  12.1 MBytes  10.1 Mbits/sec  4.759 ms  3/1541 (0.19%)  
[  4] Sent 1541 datagrams
CPU Utilization: local/receiver 1.3% (0.2%u/1.1%s), remote/sender 2.5% (0.3%u/2.2%s)

iperf Done.
