iperf 3.0.11
Linux josue-X23 4.10.0-28-generic #32~16.04.2-Ubuntu SMP Thu Jul 20 10:19:48 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux
Time: Sat, 15 Dec 2018 23:00:34 GMT
Connecting to host 192.168.43.208, port 5201
Reverse mode, remote host 192.168.43.208 is sending
      Cookie: josue-X23.1544914834.965732.38bae810
[  4] local 192.168.43.14 port 57696 connected to 192.168.43.208 port 5201
Starting Test: protocol: UDP, 1 streams, 8192 byte blocks, omitting 0 seconds, 10 second test
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-1.00   sec  1.20 MBytes  10.1 Mbits/sec  2.652 ms  0/154 (0%)  
[  4]   1.00-2.00   sec  1.19 MBytes  9.96 Mbits/sec  2.301 ms  0/152 (0%)  
[  4]   2.00-3.00   sec  1.20 MBytes  10.0 Mbits/sec  2.665 ms  0/153 (0%)  
[  4]   3.00-4.00   sec  1.19 MBytes  9.96 Mbits/sec  2.621 ms  0/152 (0%)  
[  4]   4.00-5.00   sec  1.19 MBytes  9.96 Mbits/sec  3.380 ms  1/153 (0.65%)  
[  4]   5.00-6.00   sec  1.07 MBytes  8.98 Mbits/sec  1.399 ms  0/137 (0%)  
[  4]   6.00-7.00   sec   976 KBytes  7.99 Mbits/sec  4.709 ms  0/122 (0%)  
[  4]   7.00-8.00   sec  1.13 MBytes  9.51 Mbits/sec  4.854 ms  38/183 (21%)  
[  4]   8.00-9.00   sec  1.12 MBytes  9.37 Mbits/sec  5.098 ms  10/153 (6.5%)  
[  4]   9.00-10.00  sec  1.20 MBytes  10.1 Mbits/sec  8.385 ms  0/154 (0%)  
- - - - - - - - - - - - - - - - - - - - - - - - -
Test Complete. Summary Results:
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  4]   0.00-10.00  sec  11.9 MBytes  10.0 Mbits/sec  10.081 ms  50/1516 (3.3%)  
[  4] Sent 1516 datagrams
CPU Utilization: local/receiver 1.2% (0.1%u/1.1%s), remote/sender 2.5% (0.2%u/2.3%s)

iperf Done.
