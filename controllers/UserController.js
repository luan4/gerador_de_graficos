class UserController {

    // constructor( url , element ){
    constructor(url, element) {
        // this.grafic = document.getElementById(elemente);
        this.url = url;
        this.element = element;


        this.qtsDeUsuarios();
    }


    qtsDeUsuarios() {

        let qtd_users = document.querySelector(this.element);


        fetch(this.url, {
            cache: 'reload' // definir o tipo de cache
        }).then(function (response) {
            if (response.ok) {
                // texto simples
                return response.text();
                // para transformar o `json` que é `string` para um `object`
                // javascript use `response.json()` que o próximo `then` retornará um `object`
            }
            // em caso de resposta "opaca" ou outros erros
            throw new Error('Fetch error, status code: ' + response.status);
        }).then(function (text) {

            // console.log(text);

            qtd_users.innerHTML = text;
        }).catch(function (error) {
            // caso haja um erro... tratar aqui
        });

    };


}