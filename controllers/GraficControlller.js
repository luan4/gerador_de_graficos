class GraficController {

    // constructor( url , element ){
    constructor(url , elemento , nome) {
        // this.grafic = document.getElementById(elemente);
        this.nome = nome;
        this.url = url;
        this.elemento = elemento;
        console.log(this.elemento);
        this.gerarGrafico(this.elemento , this.nome);
    }



    gerarGrafico( elemento , nome ) {

        console.log(elemento);
        fetch(this.url, {
            cache: 'reload' // definir o tipo de cache
        }).then(function (response) {
            if (response.ok) {
                // texto simples
                return response.text();
                // para transformar o `json` que é `string` para um `object`
                // javascript use `response.json()` que o próximo `then` retornará um `object`
            }
            // em caso de resposta "opaca" ou outros erros
            throw new Error('Fetch error, status code: ' + response.status);
        }).then(function (text) {

            
            console.log(text);

            // buscar elemento (div)
            // var el = document.getElementById(this.elemento);
        
            var el = document.getElementById('container');
            // adicionar resultado (objeto para string)
            // el.innerHTML = el.innerHTML += '<br>' + text;

            /*
             pega o string no formato 10 10,20 20, 30 30
             e separa as coordenadas em pares x e y
             10 10 
             20 20
             */
            let XY = text.split('\n');
            console.log(XY);

            // vai guarda todo os pontos do vetor
            var novoVetor = [];


            for (var i = 0; i < (XY.length - 1); i++) {
                /* 
                agora que as coordenadas estão quebradas em pares de x e y
                10 10
                20 20 
                ...
                vai pegar cada ponto de x e y separadamente
                10 10 vai virar um ponto no novoVetor 
                { x : 10 , y : 10}
                ṕara pode gerar cada ponto
                */
                // paga o valor de x e y 

                let valores = XY[i].split(';');

                console.log(valores)
                // console.log( parseInt(XY[i]));
                // cria o novo ponto no vetor para exebir no gráfico
                novoVetor.push({ x: parseInt(valores[0]), y: parseInt(valores[1]) });
                // novoVetor.push({ x: i+1 , y: parseInt(XY[i]) });
            }


            // caso tenha ',' no final da string
            // se não tiver comentar a linha abaixo
            novoVetor.pop(1);




            var options = {
                title: {
                    text: nome
                },
                animationEnabled: true,
                exportEnabled: true,
                data: [
                    {
                        type: "spline", //change it to line, area, column, pie, etc
                        // cria o gráfico com o novo vetor
                        dataPoints: novoVetor
                    }
                ]
            };

            
            $(elemento).CanvasJSChart(options);
            console.log(elemento);


        }).catch(function (error) {
            // caso haja um erro... tratar aqui
        });

    }

}